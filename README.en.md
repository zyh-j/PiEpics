# Pi_EPICS

#### Description
在树莓派上创建EPICS
```
su # 获取管理员权限
```

```sh
tar -zxvf base-3.15.9.tar.gz -C /opt

echo "export EPICS_BASE=/opt/base-3.15.9" >> /etc/profile
echo "export EPICS_HOST_ARCH=\$(\$EPICS_BASE/startup/EpicsHostArch)" >> /etc/profile
echo "export PATH=\$PATH:\$EPICS_BASE/bin/\$EPICS_HOST_ARCH" >> /etc/profile
echo "export EPICS_CAS_INTF_ADDR_LIST=127.0.0.1" >> /etc/profile
```

```sh
apt install  screen vim ./wiringPi....deb
```

