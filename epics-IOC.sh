#!/bin/sh

case "$1" in
        start)
                echo -n "Starting EPICS_IOC"
                
                cd /home/vivid/local_ioc/iocBoot/myioc
                screen -d -m -S ioc -h 1000 ./st.cmd
                echo -n "finished run st.cmd"
                echo "."
                ;;
        stop)
                echo -n "Stopping EPICS IOC"
                /usr/bin/killall -9 st.cmd
                echo "."
                ;;
        reload)
                echo "Not implemented"
                ;;
        force-reload|restart)
                sh $0 stop
                sh $0 start
                ;;
        *)
                echo "Usage: /etc/init.d/epics-IOC"
                Unknown macro : {start| stop|restart|force-load|reload}
        exit 1
        ;;
esac


