#!../../bin/linux-arm/myioc

## You may have to change myioc to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/myioc.dbd"
myioc_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords "db/dbSubExample.db"

## Set this to see messages from mySub
var Debug 1

## Run this to trace the stages of iocInit
#traceIocInit

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncExample, "user=vivid"
